import unittest
from cohete_mega_avanzado import Cohete


class TestComplejos(unittest.TestCase):

    def test_construir_cohete(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.numero, 11)

    def test_posicion_x(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.x, 2)

    def test_posicion_y(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.y, 3)

    def test_velocidad(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.velocidad, 3300)

    def test_despegue(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.despegue(), "Despegamos en la posición (0,0) en 3,2,1..." )

    def test_aterrizaje(self):
        Apolo_11 = Cohete(11,2,3,3300)

        self.assertEqual(Apolo_11.aterrizar(), "El Apolo 11 ha aterrizado" )
    def test_movimiento(self):
        Apolo_11 = Cohete(11,2,3,3300)

        Apolo_11.movimiento_cohete()

        self.assertEqual(Apolo_11.x, 3)
        self.assertEqual(Apolo_11.y, 4)
    def test_distancia_cohetes(self):
        Apolo_11 = Cohete(11,2,3,3300)
        Apolo_12 = Cohete(12,5,80,3300)

        distancia = Apolo_11.get_distance(Apolo_12) 

        #self.assertEqual(distancia ,"La distancia entre los dos cohetes es de:(3, 77)" )
        self.assertEqual(distancia ,(3, 77) )




        


        








if __name__ == "__main__":
    unittest.main()