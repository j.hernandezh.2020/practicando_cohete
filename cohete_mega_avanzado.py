class Cohete():
    def __init__(self,numero,x=0,y=0, velocidad= 30000):
        self.x = x
        self.y = y
        self.numero =numero
        self.velocidad = velocidad
    
    def movimiento_cohete(self):
        self.x += 1
        self.y += 1

    def get_distance(self,cohete2):
        distancia_x = cohete2.x - self.x
        distancia_y = cohete2.y - self.y
        distancia_total = (distancia_x,distancia_y)
        return distancia_total
        #return"La distancia entre los dos cohetes es de:{distance}".format(distance = distancia_total)
    def distancia_de_seguridad(self,cohete2):
        dame_diferencia_distancia = self.get_distance(cohete2)
        if dame_diferencia_distancia==(0,0):
            return "BOOOOM los cohetes se chocaron"
        else:
            return "Los cohetes se encuentran a una distancia de ({x})".format(x =dame_diferencia_distancia)
    
    def despegue(self):
        self.x = 0
        self.y = 0
        return "Despegamos en la posición ({x},{y}) en 3,2,1...".format(x = self.x, y=self.y)
    def aterrizar(self):
        return "El Apolo {numero} ha aterrizado".format(numero=self.numero)
    def __str__(self):
       return "La posición del Apolo {numero} es ({x},{y}) con una velocidad de {vel}km/h".format(numero = self.numero, x =self.x,y = self.y,vel= self.velocidad)


