class Cohete():
    def __init__(self,numero,x=0,y=0, velocidad= 30000):
        self.x = x
        self.y = y
        self.numero =numero
        self.velocidad = velocidad
    

    def movimiento_cohete(self):
        self.x += 1
        self.y += 1
    def get_distance(self,cohete2):
        distancia_x = cohete2.x - self.x
        distancia_y = cohete2.y - self.y
        distantancia_total = (distancia_x,distancia_y)
        return distantancia_total

    def __str__(self):
       return "La posición del Apolo {numero} es ({x},{y}) con una velocidad de {vel}km/h".format(numero = self.numero, x =self.x,y = self.y,vel= self.velocidad)
Apolo_11 = Cohete(11,1,1,33333)
Apolo_12 = Cohete(12,3,-2)
Apolo_13 = Cohete(13,5,12,3333)
print(Apolo_12)
distancia = Apolo_11.get_distance(Apolo_13)
print(distancia)
